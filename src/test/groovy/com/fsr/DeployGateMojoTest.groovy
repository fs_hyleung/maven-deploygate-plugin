import spock.lang.Specification
import com.fsr.*
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.mime.MultipartEntity
import org.apache.maven.plugin.MojoExecutionException
import org.apache.http.HttpResponse
import org.apache.http.StatusLine
import org.apache.http.HttpStatus

class DeployGateMojoTest extends Specification{
    def mojo
    def HttpClient httpClient;
    def response
    def statusLine
    def setup() {
        mojo = new DeployGateMojo();
        mojo.ownerName='fsrdemo'
        mojo.apiKey = '8a28512e349a1cc183c332141c9e324f51ef4324'
        //stub out the http client
        httpClient = Mock(HttpClient)
        mojo.httpClient = httpClient
        response = Mock(HttpResponse)
        statusLine = Mock(StatusLine)
        statusLine.getStatusCode() >> 200
        response.getStatusLine() >> statusLine
        httpClient.execute(_) >> response
    }

    def "execute mojo should do http POST"() {
        given:
            mojo.apkFile = new File('foo.apk')

        when:
            mojo.execute()
        then:
            1 * httpClient.execute(_) >> response
    }

    def "execute mojo should http post with entity"() {
        given:
            mojo.apkFile = new File('foo.apk')
        when:
            mojo.execute()
        then:
           1 * httpClient.execute(!null) >> response
    }

    def "execute mojo should http post with form entity"() {
        given:
        mojo.apkFile = new File('foo.apk')
        when:
        mojo.execute()
        then:
            1 * httpClient.execute({it.getEntity() instanceof MultipartEntity}) >> response
    }

    def "execute mojo throws exception"() {
        given:
            mojo.apkFile = new File('foo.apk')
            httpClient = Mock(HttpClient)
            mojo.httpClient = httpClient
            httpClient.execute(_) >> {throw new IOException()}
        when:
            mojo.execute()
        then:
            thrown(MojoExecutionException)
    }

    def "HTTP error throws exception"(int x) {
        given:
            mojo.apkFile = new File('foo.apk')
        when:
            mojo.execute()
        then:
            statusLine.getStatusCode() >> x
        then:
            thrown(MojoExecutionException)
        where:
            x << [
                    HttpStatus.SC_INTERNAL_SERVER_ERROR,
                    HttpStatus.SC_BAD_REQUEST,
                    HttpStatus.SC_UNAUTHORIZED,
                    HttpStatus.SC_FORBIDDEN,
                    HttpStatus.SC_NOT_FOUND
                ]
    }

}

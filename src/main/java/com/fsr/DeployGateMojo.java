package com.fsr;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.File;
import java.io.IOException;

/**
 * Goal that pushes an APK to DeployGate
 *
 * @goal push
 */
public class DeployGateMojo
    extends AbstractMojo
{
    /**
     * Location of the file.
     * @parameter
     * @required
     */
    private File apkFile;
    /**
     * DeployGate API key
     * @required
     * @parameter
    */
    private String apiKey;
    /**
    * @parameter
    * @required
    */
    private String ownerName;
    /**
     * @parameter
     */
    private String comment;
    private HttpClient httpClient = new DefaultHttpClient();
    public void execute()
        throws MojoExecutionException
    {
        String url = String.format("https://deploygate.com/api/users/%s/apps",ownerName);
        getLog().info(String.format("Executing POST to %s",url));
        getLog().info(String.format("APK file: %s",apkFile.getPath()));
        getLog().info(String.format("Token: %s", apiKey));
        HttpPost post = new HttpPost(url);
        try {
            MultipartEntity entity = new MultipartEntity();
            entity.addPart(new FormBodyPart("file",new FileBody(apkFile)));
            entity.addPart(new FormBodyPart("token",new StringBody(apiKey)));
            if(comment!=null)
                entity.addPart(new FormBodyPart("message",new StringBody(comment)));
            post.setEntity(entity);
            HttpResponse response = httpClient.execute(post);
            int statusCode = response.getStatusLine().getStatusCode();
            switch(statusCode) {
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                case HttpStatus.SC_NOT_FOUND:
                case HttpStatus.SC_BAD_REQUEST:
                case HttpStatus.SC_FORBIDDEN:
                case HttpStatus.SC_UNAUTHORIZED:
                    String message = String.format("Error: DeployGate API returned %d", statusCode);
                    throw new MojoExecutionException(message);
                default:
                    getLog().info(String.format("DeployGate API returned %d",statusCode));
            }
        }
        catch(IOException e) {
            throw new MojoExecutionException( "Error pushing apk to DeployGate " + e.getMessage(), e );
        }

    }
}
